package com.example.soundboardey.utilities

import android.media.ToneGenerator
import com.example.soundboardey.R

fun getSoundByName(name: String): Int {
    return when (name) {
        "imgBtn_zdravkoM" -> R.raw.kids_laugh
        "imgBtn_markZ" -> R.raw.alien
        "imgBtn_elonM" -> R.raw.fake_applause
        else -> ToneGenerator.TONE_CDMA_PIP
    }
}