package com.example.soundboardey.sounds

interface AudioPlayer {
    fun playPersonSound(soundId: Int)
}