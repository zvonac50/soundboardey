package com.example.soundboardey

import android.app.Application
import com.example.soundboardey.di.appModule
import com.example.soundboardey.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SoundBoardey : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SoundBoardey)
            modules(appModule, viewModelModule)
        }
    }
}