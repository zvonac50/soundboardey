package com.example.soundboardey.ui

import androidx.lifecycle.ViewModel
import com.example.soundboardey.sounds.AudioPlayer
import com.example.soundboardey.utilities.getSoundByName

class PersonViewModel(
    private val audioPlayer: AudioPlayer
) : ViewModel() {

    fun playPersonSound(soundId: String) {
        audioPlayer.playPersonSound(getSoundByName(soundId))
    }
}