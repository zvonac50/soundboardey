package com.example.soundboardey.di

import com.example.soundboardey.sounds.AudioPlayer
import com.example.soundboardey.sounds.SoundPoolPlayer
import com.example.soundboardey.ui.PersonViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single<AudioPlayer> { SoundPoolPlayer(androidContext()) }
}

val viewModelModule = module {
    viewModel<PersonViewModel> { PersonViewModel(get()) }
}